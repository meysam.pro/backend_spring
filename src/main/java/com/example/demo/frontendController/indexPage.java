package com.example.demo.frontendController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class indexPage {
  @GetMapping("/")
  public String signing() {
    return "login/signin";
  }

  @GetMapping("/signup")
  public String signup() {
    return "login/signup";
  }

  @GetMapping("/port")
  public String portfolio() {
    return "portfolio/index";
  }

}

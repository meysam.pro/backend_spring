testAll:
	./gradlew clean test


buildAndrun:
	./gradlew clean build -x test
	docker-compose down
	docker-compose build
	docker-compose up &
